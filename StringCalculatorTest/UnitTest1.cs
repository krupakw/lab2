using NUnit.Framework;
using Calculator;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestStringCalculator_Calculate_EmptyString_ReturnsZero()
        {
            var res = StringCalculator.Calculate("");
            Assert.Zero(res);
        }

        [Test]
        [TestCase("365",365)]
        [TestCase("26", 26)]
        public void TestStringCalculator_Calculate_SingleNumbrer_ReturnTheValue(string s, int v)
        {
            var res = StringCalculator.Calculate(s);
            Assert.AreEqual(res,v);
        }

        [Test]
        [TestCase("365,1", 366)]
        public void TestStringCalculator_Calculate_TwoStringsSeparatedByComma_ReturnSum(string s, int v)
        {
            var res = StringCalculator.Calculate(s);
            Assert.AreEqual(res, v);
        }

        [Test]
        [TestCase("365\n1", 366)]
        public void TestStringCalculator_Calculate_TwoStringsSeparatedByNewLine_ReturnSum(string s, int v)
        {
            var res = StringCalculator.Calculate(s);
            Assert.AreEqual(res, v);
        }

        [Test]
        [TestCase("365\n1,1,2\n1", 370)]
        public void TestStringCalculator_Calculate_ManyNumbersDelimitedEitherWay_ReturnSum(string s, int v)
        {
            var res = StringCalculator.Calculate(s);
            Assert.AreEqual(res, v);
        }


        [Test]
        [TestCase("365\n1,1,-2\n1")]
        public void TestStringCalculator_Calculate_NegativeNumbers_ThrowsException(string s)
        {
            Assert.Throws<System.Exception>(()=> StringCalculator.Calculate(s));
        }

        [Test]
        [TestCase("1001", 0)]
        [TestCase("5,1001", 5)]
        [TestCase("365\n25252525,1,3002\n1", 367)]
        public void TestStringCalculator_Calculate_NumbersGreaterThan1000_AreIgnored(string s, int v)
        {
            var res = StringCalculator.Calculate(s);
            Assert.AreEqual(res, v);
        }

        [Test]
        [TestCase("//#365#2",367)]
        public void TestStringCalculator_Calculate_SingleCharSeparatorCanBeDefined_ReturnSum(string s, int v)
        {
            var res = StringCalculator.Calculate(s);
            Assert.AreEqual(res, v);
        }

        //[Test]
        //[TestCase("//[##]365##2", 367)]
        //public void TestStringCalculator_Calculate_MultiCharSeparator_ReturnSum(string s, int v)
        //{
        //    var res = StringCalculator.Calculate(s);
        //    Assert.AreEqual(res, v);
        //}

        [Test]
        public void Test()
        {
            var s = "//#2";
            Assert.AreEqual(s[2], '#');
        }
    }
}