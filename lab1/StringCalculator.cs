﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public static class StringCalculator
    {
        public static int Calculate(string s)
        {
            int res = 0;
            var delims = new[] { ",", "\n" };
            if (s.StartsWith("//"))
            {
                if (s.StartsWith("//["))
                {
                    delims = delims.Concat(new[] { s[2].ToString() }).ToArray();
                    s = s.Remove(0, 3);
                }
                else
                {
                    delims = delims.Concat(new[] { s[2].ToString() }).ToArray();
                    s = s.Remove(0, 3);
                }
            }

            var numbers = s.Split(delims,StringSplitOptions.RemoveEmptyEntries);

            foreach(var num in numbers)
            {
                int n;
                if (Int32.TryParse(num, out n)&& n<=1000)
                {
                    if (n < 0)
                        throw new Exception("Negative number: "+ n);
                    res += n;
                }
            }
            return res;
        }
    }
}
